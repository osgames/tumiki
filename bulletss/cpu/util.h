#ifndef BOHA_UTIL
#define BOHA_UTIL

#include "cpputil.h"

#include <string>
#include <stdexcept>
#include <cmath>
#include <iostream>
#include <cassert>

const double PI = 3.1415926;
const double SQRT_TWO = std::sqrt(2.0);

const double PI_PER_2 = PI / 2;
const double PI_PER_4 = PI / 4;
const double PI_PER_8 = PI / 8;

double dtor(double x);
double rtod(double x);

template <typename T_>
inline T_ absolute(T_ v) {
	return (v < 0) ? -v : v;
}

void ignoreBrankAndComment(std::string& str, std::string delim = "#");

struct GetDoubleCastPolicy {
	template <typename T_>
	static double getDouble(T_ v) { return static_cast<double>(v); }
};

struct GetDoubleMethodPolicy {
	template <typename T_>
	static double getDouble(T_ v) { return v.getDouble(); }
};

template <typename T_, class GetDoublePolicy_ =GetDoubleCastPolicy>
struct Coord
{
	T_ x, y;

	Coord() {}
	Coord(T_ x0, T_ y0) : x(x0), y(y0) {}
	template <typename T2_, class GetDoublePolicy2_>
	Coord(const Coord<T2_, GetDoublePolicy2_>& rhs) : x(rhs.x), y(rhs.y) {}
	explicit Coord(const std::string& rhs);

    const Coord operator+ (const Coord& rhs) const {
		Coord p(x+rhs.x, y+rhs.y);
		return p;
    }
    const Coord operator- (const Coord& rhs) const {
		Coord p(x-rhs.x, y-rhs.y);
		return p;
    }
    const Coord operator* (const T_ rhs) const {
		Coord p(x*rhs, y*rhs);
		return p;
    }
    const Coord operator/ (const T_ rhs) const {
		Coord p(x/rhs, y/rhs);
		return p;
    }
    const Coord& operator+= (const Coord& rhs) {
		x += rhs.x;
		y += rhs.y;
		return *this;
    }
    const Coord& operator-= (const Coord& rhs) {
		x -= rhs.x;
		y -= rhs.y;
		return *this;
    }
    const Coord& operator*= (const T_ rhs) {
		x *= rhs;
		y *= rhs;
		return *this;
    }
    const Coord& operator/= (const T_ rhs) {
		x /= rhs;
		y /= rhs;
		return *this;
    }
    const Coord operator-() {
		Coord p(-x, -y);
		return p;
    }

	friend std::ostream& operator<< (std::ostream& os, const Coord<T_>& rhs) {
		os << '(' << rhs.x << ',' << rhs.y << ')';
		return os;
	}

	T_ length2() const {
		return x*x+y*y;
	}
	T_ length2(const Coord& p) const {
		T_ x1 = x-p.x;
		T_ y1 = y-p.y;
		return x1*x1+y1*y1;
	}

    double length() const {
		return std::sqrt(GetDoublePolicy_::getDouble(length2()));
	}
    double length(const Coord& p) const {
		return std::sqrt(GetDoublePolicy_::getDouble(length2(p)));
	}

	void rotate(double angle);

    void unit();

	/**
	 * 上が 0度で右回り。
	 */
    double angle() const;
    double angle(const Coord& p) const {
		Coord vec(*this - p);
		return vec.angle();
	}

    double innerProduct() const {
		return x*x+y*y;
    }

    int xi() const { return static_cast<int>(x); }
    int yi() const { return static_cast<int>(y); }
	double xd() const { return GetDoublePolicy_::getDouble(x); }
	double yd() const { return GetDoublePolicy_::getDouble(y); }

};

typedef Coord<double> Point;

template <typename T_, class GetDoublePolicy_>
Coord<T_, GetDoublePolicy_>::Coord(const std::string& rhs) {
	if (rhs.empty()) {
		x = 0;
		y = 0;
		return;
	}

	size_t index = rhs.find('x');
	if (index == std::string::npos) index = rhs.find(' ');
	assert(index != std::string::npos);

	x = atoi(rhs.substr(0, index));
	y = atoi(rhs.substr(index+1));
}

template <typename T_, class GetDoublePolicy_>
double Coord<T_, GetDoublePolicy_>::angle() const {
	double ret;
    if (y == 0) {
		if (x > 0) return PI_PER_2;
		else return PI_PER_2 * 3;
	}
    else ret = std::atan(GetDoublePolicy_::getDouble(x/y));
	return (y > 0) ? PI-ret : -ret;
}

template <typename T_, class GetDoublePolicy_>
void Coord<T_, GetDoublePolicy_>::unit() {
	T_ x2 = absolute(x) * x;
	T_ y2 = absolute(y) * y;
    T_ x2y2 = absolute(x2)+absolute(y2);
    x = x2 / x2y2;
    y = y2 / x2y2;
}

template <typename T_, class GetDoublePolicy_>
void Coord<T_, GetDoublePolicy_>::rotate(double angle) {
	double cs = cos(angle);
	double sn = sin(angle);
	T_ x2 = cs * x - sn * y;
	y = sn * x + cs * y;
	x = x2;
}

#endif // BOHA_UTIL
