import windows;
import string;

extern (C) void gc_init();
extern (C) void gc_term();
extern (C) void _minit();
extern (C) void _moduleCtor();
extern (C) void _moduleUnitTests();

extern (C) int doit(char[][] args);

extern (Windows) int WinMain(HINSTANCE hInstance,
							 HINSTANCE hPrevInstance,
							 LPSTR lpCmdLine,
							 int nCmdShow)
{
	int result;

	gc_init();          // ガベージコレクタ初期化
	_minit();           // モジュールコンストラクタテーブル初期化
    
	try
	{
        _moduleCtor();      // モジュールコンストラクタ呼び出し
        _moduleUnitTests(); // 単体テスト実行 (オプション)

		char[] arg = string.toString(lpCmdLine);
		char exe[4096];
		GetModuleFileNameA(null, exe, 4096);
		char[][] args;
		args ~= string.toString(exe);
		args ~= split(arg);

        result = doit(args);    // ここにユーザーコードを置きます
	}
    
	catch (Object o)        // キャッチされていない例外を全てキャッチ
	{
        MessageBoxA(null, (char *)o.toString(), "Error",
					MB_OK | MB_ICONEXCLAMATION);
        result = 0;     // failed
	}
   
	gc_term();          // ファイナライザの実行、ガベージコレクタ終了
	return result;
}
