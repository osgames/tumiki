弾幕 360 度 ver 0.0.1  &  libBulletML for D

縦シューって普通見下ろし視点なんですけど、
色んな角度から見てみようという話。
いかにパイロットが優秀かがわかるかも。

SDL.dll と弾幕データを別途必要とします。
というか、白い弾幕くんを落っことして、
sdmkun ディレクトリ(フォルダ)に
bulletss.exe, bulletml.dll, cpu.dll を
移動させて bulletss.exe をダブルクリック、
という起動法が想定されています。

起動したらエスケープキーで終了できます。
ALT-F4 で終了できないのは問題だと思いますんで
いずれなんとかするかもしれません。

技術的な話。

たぶん今をときめく D 言語で書かれています。

拙作 d_cpp やそれを用いて、 libBulletML、
議会制弾幕回避機関が動いているサンプルになるかと思います。

libBulletML for D と別配布するのは保守の手間が割と大変なので、
これを同時に libBulletML for D のパッケージとさせて頂きます。

getRand は set してもしなくても良いです。
しない場合は C 標準ライブラリの rand が用いられます。
その場合の seed を変える処理は bulletss.d が参考になります。
逆に getRand をセットする場合は test_bulletml.d を見て下さい。

Windows で D の部分をコンパイルするには、 D コンパイラ dmd、DigiralMars の C コ
ンパイラ sc、 DigitalMars もしくは BorlandC++ 付属の (つまるところ OMF を扱え
る) implib、 GNU make が必要です。 GNU make は私が cygwin でコンパイルするため
に使っているものですが、適当に Makefile.win32 を書きかえれば他の環境でもなんと
かなるでしょう。また import library であるところの SDL.lib と opengl32.lib は 
DedicateD から持ってきて下さい。 user32.lib や kernel32.lib は dmc 付属のもの、
phobos.lib は dmd 付属のものをお使い下さい。

一方 bulletml や cpu ディレクトリの下は BorlandC++ でコンパイルします。本当は
DigitalMars のものでやりたかったのですが、 C++ の対応がいまひとつだったように見
受けられたので、 BorlandC++ でコンパイルし、dll を作り、その import library を
作って本体とリンクします。この場合のビルドは bulletml/tinyxml, bulletml, cpu の
三つのディレクトリで BorlandC++ 付属の make を Makefile.bcc32 を使って起動すれ
ば OK です。

Linux では恐らく随分簡単で、 gcc と dmd が入ってれば後は make 一発じゃないかと
思います。

cpu.d, bulletml.d は perl で生成されています。 perl があれば cpu/cpu_d.cpp,
bulletml/bulletml_d.cpp から d_cpp を用いて生成できると思います。 Linux の
Makefile を見ればいくらかわかるかと。

d_cpp は def ファイルも吐くと便利だと思いました。

opengl.d は linux のことを考えてくれてなかったので書きかえてます。

mesaglu.d は glu32.lib が動くまでの間に合わせです。 Mesa を使ったため MIT ライ
センスに従って下さい。

libBulletML は例のごとく修正 BSD ライセンスです。

残りは GPL2 です。ですが、winmain.d は DigitalMars のサイトを全面的に参考にして
ますし、こいつはご自由にご利用下さい。

------------------
 shinichiro.h
  s31552@mail.ecc.u-tokyo.ac.jp
  http://user.ecc.u-tokyo.ac.jp/~s31552/wp/
