import random;
import math;

import opengl;
import mesaglu;

import bulletss;
import charactor;

class Camera {
public:
	this() {
		cameras_ ~= new NormalCamera();
		cameras_ ~= new UpRollCamera();
		cameras_ ~= new PlayerCamera();
		cameras_ ~= new EnemyCamera();
		cameras_ ~= new PlayerToEnemyCamera();
		cameras_ ~= new RandomCamera();
		cameras_ ~= new SideCamera();
		cameras_ ~= new PlayerUpCamera();
		cameras_ ~= new GrazeUpRollCamera();
		cameras_ ~= new RandomLineCamera();

		camera_ = cameras_[rnd(cameras_.length)];
		camera_.reset();

		ex_ = 320;
		ey_ = 240;
		ez_ = -500;
		cx_ = 320;
		cy_ = 240;
		cz_ = 0;
		ux_ = 0;
		uy_ = -1;
		uz_ = 0;
	}

	void set() {
		camera_.set(ex_, ey_, ez_, cx_, cy_, cz_, ux_, uy_, uz_);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluLookAt(ex_, ey_, ez_, cx_, cy_, cz_, ux_, uy_, uz_);

		if (camera_.isEnd()) {
			camera_ = cameras_[rnd(cameras_.length)];
			camera_.reset();
		}
	}

private:
	CameraImpl camera_;
	CameraImpl[] cameras_;

	float ex_, ey_, ez_, cx_, cy_, cz_, ux_, uy_, uz_;
}

class CameraImpl {
public:
	void set(inout float ex, inout float ey, inout float ez,
			 inout float cx, inout float cy, inout float cz,
			 inout float ux, inout float uy, inout float uz) {
		ex_ = ex; ey_ = ey; ez_ = ez;
		cx_ = cx; cy_ = cy; cz_ = cz;
		ux_ = ux; uy_ = uy; uz_ = uz;

		set_();

		ex = ex_; ey = ey_; ez = ez_;
		cx = cx_; cy = cy_; cz = cz_;
		ux = ux_; uy = uy_; uz = uz_;

		turn_++;
	}
	bit isEnd() {
		return turn_ > 600;
	}
	void reset() {
		turn_ = 0;
	}

	abstract void set_();

	void setAim(float ex, float ey, float ez,
				float cx, float cy, float cz,
				float ux, float uy, float uz, int times)
	in {
		assert(times > 0);
	}
	body {
		dex_ = (ex - ex_)  / times;
		dey_ = (ey - ey_)  / times;
		dez_ = (ez - ez_)  / times;
		dcx_ = (cx - cx_)  / times;
		dcy_ = (cy - cy_)  / times;
		dcz_ = (cz - cz_)  / times;
		dux_ = (ux - ux_)  / times;
		duy_ = (uy - uy_)  / times;
		duz_ = (uz - uz_)  / times;
	}
	void goAim() {
		ex_ += dex_;
		ey_ += dey_;
		ez_ += dez_;
		cx_ += dcx_;
		cy_ += dcy_;
		cz_ += dcz_;
		ux_ += dux_;
		uy_ += duy_;
		uz_ += duz_;
	}

protected:
	int turn_;
	float ex_, ey_, ez_, cx_, cy_, cz_, ux_, uy_, uz_;
	float dex_, dey_, dez_, dcx_, dcy_, dcz_, dux_, duy_, duz_;
}

class NormalCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 240, -500, 320, 240, 0, 0, -1, 0, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
		}
	}
}

class UpRollCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 240, -500, 320, 240, 0, 0, -1, 0, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
			float t = (turn_ - 100) * 0.01;
			ux_ = math.sin(t);
			uy_ = -math.cos(t);
		}
	}
}

class PlayerCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 480, -20, 320, 240, 0, 0, -20, -240, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
		}
	}
}

class EnemyCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 0, -20, 320, 240, 0, 0, 20, -240, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
		}
	}
}

class PlayerToEnemyCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 480, -20, 320, 240, 0, 0, -20, -240, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
			Charactor p = BulletSS.obj.target();
			Charactor e = BulletSS.obj.topBullet();
			setAim(p.x()+170, p.y()+40, -30,
				   e.x()+170, e.y()+40, 0, 0, -30, -100, 5);
			goAim();
		}
	}
}

class RandomCamera : public CameraImpl {
	void set_() {
		if (turn_ % 100 == 0) {
			setAim(rnd(640), rnd(480), rnd(50)-25,
				   320, 240, 0, rnd(3)-1, rnd(3)-1, rnd(3)-1, 100);
		}
		goAim();
	}
}

class SideCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(650, 240, -50, 320, 240, 0, 50, 0, -330, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
		}
	}
}

class PlayerUpCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 240, -500, 320, 240, 0, 0, -1, 0, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
			Charactor p = BulletSS.obj.target();
			setAim(p.x()+170, p.y()+90, -100,
				   p.x()+170, p.y()+40, 0,
				   0, -10, -5, 5);
			goAim();
		}
	}
}

class GrazeUpRollCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			setAim(320, 480, 240, 320, 240, 0, 0, -1, 1, 100);
		}
		else if (turn_ < 100) {
			goAim();
		}
		else {
			float t = (turn_ - 100) * -0.01;
			ex_ = 320 + math.sin(t) * 240;
			ey_ = 240 + math.cos(t) * 240;
			ux_ = -math.sin(t);
			uy_ = -math.cos(t);
		}
	}
}

class RandomLineCamera : public CameraImpl {
	void set_() {
		if (turn_ == 0) {
			dex_ = rnd(3)-1;
			dey_ = rnd(3)-1;
			dez_ = rnd(3)-1;
			dcx_ = 0;
			dcy_ = 0;
			dcz_ = 0;
			dux_ = 0;
			duy_ = 0;
			duz_ = 0;
			cx_ = 320;
			cy_ = 240;
			cz_ = 0;
		}
		goAim();
	}
}

