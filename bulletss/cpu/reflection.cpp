#include "reflection.h"

#include "cpuutil.h"
#include "cpuinfo.h"

#include <cmath>
#ifndef M_PI
# define M_PI           3.14159265358979323846  /* pi */
#endif

ReflectionCpu::ReflectionCpu() {
	name_ = "reflection";
}

void ReflectionCpu::calc() {
    Point mypnt = info_->getPlayerPnt();
    PosSpds posspd;
    info_->getBulletsPosAndSpd(posspd);

	std::vector<Point> mypnts;
	for (int i = 0; i < 9; i++) {
		mypnts.push_back(
			getMovedPoint(mypnt, i));
	}

    double dp = -1;
    int index, minIndex =0;

	double dps[9];
	double dpAll = 0;

    for (index = 0; index < 9; index++) {
		const Point& upper = info_->getPlayerMaxPnt();

		Point& pnt = mypnts[index];
		if (pnt.x > upper.x || pnt.x < 0 || pnt.y > upper.y || pnt.y < 0)
			continue;

		double nowDp = dangerPoint(pnt, posspd);

		// 前回のまんまの行動は比較的自然だろう
		//if (index == prevIndex_ && nowDp != 0.0) nowDp *= 0.9;
		if (index == 0 && nowDp != 0.0) nowDp *= 0.95;

		dps[index] = nowDp;
		dpAll += nowDp;

		if (nowDp < dp || dp == -1) {
			dp = nowDp;
			minIndex = index;
		}

		// std::cerr << index << ": " << nowDp << std::endl;
    }

	axis_ = minIndex;

    decision_ = false;
    prevIndex_ = minIndex;

	if (dpAll != 0) {
		for (int i = 0; i < 9; i++) {
			evaluations_[i] = 5 - static_cast<int>(dps[i] / dpAll * 100);
		}
	}
}

double ReflectionCpu::dangerPoint(Point mypnt, PosSpds& posspd) {
    double dp = 0;

    for (PosSpds::iterator ite = posspd.begin(); ite != posspd.end(); ite++) {
		Point& pnt = ite->first;
		Point& spd = ite->second;

		Point vec(mypnt-pnt);

		double timeLength = spd.length2()/vec.length2();
		if (timeLength <= 4) continue;

		double angle = 
			M_PI/4.0 -
			std::fabs(std::fabs(spd.angle())-std::fabs(mypnt.angle(pnt)));

		if (angle > 0)
			dp += timeLength*angle*angle;
    }

    return dp;
}

