/* Copyright (C) Steve Rabin, 2000. 
 * All rights reserved worldwide.
 *
 * This software is provided "as is" without express or implied
 * warranties. You may freely copy and compile this source into
 * applications you distribute provided that the copyright text
 * below is included in the resulting source code, for example:
 * "Portions Copyright (C) Steve Rabin, 2000"
 */

#ifndef _PROFILE_H
#define _PROFILE_H

#ifdef NDEBUG
# define PROFILE(s)
# define PROFILE_INIT
# define PROFILE_OUTPUT
#else
# define PROFILE(s) ProfileInstance profile_instance__(s);
# define PROFILE_INIT ProfileInit()
# define PROFILE_OUTPUT ProfileDumpOutputToBuffer()
#endif

void ProfileInit( void );
void ProfileBegin( const char* name );
void ProfileEnd( const char* name );
void ProfileDumpOutputToBuffer( void );
void StoreProfileInHistory(const char* name, float percent);
void GetProfileFromHistory(const char* name, float* ave,
						   float* min, float* max );

class ProfileInstance {
public:
	ProfileInstance(const char* name) : name_(name) {
		ProfileBegin(name);
	}
	~ProfileInstance() {
		ProfileEnd(name_);
	}

private:
	const char* name_;
};

#endif
