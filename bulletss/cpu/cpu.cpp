#include "cpu.h"

#include <iostream>

void CpuInputBase::message(const std::string& msg) {
	std::cout << name() << ": " << msg << std::endl;
}

void CpuInputBase::update() {
    if (info_->getTurn() == lastTurn_) return;

    lastTurn_ = info_->getTurn();

	initEvaluation();

	calc();
}

void CpuInputBase::initEvaluation() {
	for (int i = 0; i < 9; i++) {
		evaluations_[i] = 0;
	}
}

CpuInfo* CpuInputBase::info_ = 0;

#include "chairman.h"

CpuInputBase* CpuInputBase::getDefaultCpu() {
	return new ChairmanCpu();
}
