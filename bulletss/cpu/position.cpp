/**
 * $Id: position.cc,v 1.1 2003/09/06 22:53:28 i Exp $
 *
 * Copyright (C) shinichiro.h <s31552@mail.ecc.u-tokyo.ac.jp>
 *  http://user.ecc.u-tokyo.ac.jp/~s31552/wp/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "position.h"

#include "util.h"

#include <numeric>

PositionCpu::PositionCpu() {
	name_ = "position";
}

void PositionCpu::calc() {
	Point pnt = info_->getPlayerPnt();
	const Point& upper = info_->getPlayerMaxPnt();
    Point upperLen = upper - pnt;

    if (pnt.x < 30) {
		evaluations_[7] -= static_cast<int>(100 / (pnt.x*pnt.x+10));
		evaluations_[3]++;
	}
    if (pnt.y < 100) {
		evaluations_[1] -= static_cast<int>(100 / (pnt.y*pnt.y+10));
		evaluations_[5]++;
	}
    if (upperLen.x < 30) {
		evaluations_[3] -= static_cast<int>(100 / (upperLen.x*upperLen.x+10));
		evaluations_[7]++;
	}
    if (upperLen.y < 100) {
		evaluations_[5] -= static_cast<int>(100 / (upperLen.y*upperLen.y+10));
		evaluations_[1]++;
	}

	if (pnt.y < upper.y / 2) {
		evaluations_[1] -= static_cast<int>(upper.y / 2 - pnt.y) / 10;
		evaluations_[5]++;
	}

	evaluations_[2] = (evaluations_[1] + evaluations_[3]) / 2;
	evaluations_[4] = (evaluations_[3] + evaluations_[5]) / 2;
	evaluations_[6] = (evaluations_[5] + evaluations_[7]) / 2;
	evaluations_[8] = (evaluations_[7] + evaluations_[1]) / 2;

	// バランスをとるために
	evaluations_[0] = std::accumulate(evaluations_+1, evaluations_+9, 0) / 8;

}

