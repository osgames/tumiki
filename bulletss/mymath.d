import math;
import random;

int rnd(int v) {
	return random.rand() % v;
}

float rtod(float a) {
	return a * 180 / math.PI;
}
float dtor(float a) {
	return a * math.PI / 180;
}

