extern "C" {
    virtual int getAxis() {
		update();
		return axis_;
    }
    virtual bool getButton(int id) {
		update();
		return decision_ && id == 1;
	}

	virtual void registShot(const PosSpd&) {}

	virtual void registEnemy(const class Enemy*) {}

	virtual int getAxisEvaluation(int axis) {
		return evaluations_[axis];
	}

	virtual int getConfidence() const { return 100; }

	virtual void report() const {}
}

