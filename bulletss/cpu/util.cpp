#include "util.h"

#include <cmath>
#include <cstdlib>
#include <cassert>

double dtor(double x) { return x*PI/180; }
double rtod(double x) { return x*180/PI; }

