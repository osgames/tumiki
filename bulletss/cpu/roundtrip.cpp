/**
 * $Id: roundtrip.cc,v 1.2 2003/09/16 08:25:57 i Exp $
 *
 * Copyright (C) shinichiro.h <s31552@mail.ecc.u-tokyo.ac.jp>
 *  http://user.ecc.u-tokyo.ac.jp/~s31552/wp/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "roundtrip.h"

#include "cpuutil.h"
#include "util.h"

#include <algorithm>

const double RoundtripCpu::MIN_ANGLE = dtor(1);
const double RoundtripCpu::MAX_ANGLE = dtor(60);
const int RoundtripCpu::SAME_TURN_SHOTS = 5;
const int RoundtripCpu::MAX_CONFIDENCE = 10;
const int RoundtripCpu::FORGET_TURN = 120;
const size_t RoundtripCpu::MIN_BULLETS = 50;

RoundtripCpu::RoundtripCpu()
	: lastRegistShotTurn_(-1), sameTurnRegists_(0), right_(true)
{
	name_ = "roundtrip";
}

void RoundtripCpu::calc() {
	static const int RIGHT_EDGE = (int)(info_->getPlayerMaxPnt().x * 4 / 5);
	static const int LEFT_EDGE = (int)(info_->getPlayerMaxPnt().y / 5);

	// 毎ターンリセット
	sameTurnRegists_ = 0;

	int turn = info_->getTurn();
	while (!registed_.empty() && turn > registed_.front().second) {
		registed_.pop_front();
	}

	const Point& pnt = info_->getPlayerPnt();
	if ((right_ && pnt.x > RIGHT_EDGE) ||
		(!right_ && pnt.x < LEFT_EDGE))
	{
		right_ = !right_;
		// message("切りかえしー");
	}

	if (registed_.size() < MIN_BULLETS) return;

	if (right_) {
		evaluations_[2] = 10;
		evaluations_[3] = 20;
		evaluations_[4] = 10;
	}
	else {
		evaluations_[6] = 10;
		evaluations_[7] = 20;
		evaluations_[8] = 10;
	}
}

void RoundtripCpu::registShot(float x, float y, float sx, float sy) {
	int turn = info_->getTurn();

	if (lastRegistShotTurn_ == turn &&
		sameTurnRegists_ >= SAME_TURN_SHOTS)
	{
		return;
	}
	sameTurnRegists_++;

	double angle = getShotAngle(PosSpd(Point(x, y), Point(sx, sy)));

	if (angle < MIN_ANGLE || angle > MAX_ANGLE) {
		registed_.push_back(RegistedShot(false, turn + FORGET_TURN));
	}
	else {
		registed_.push_back(RegistedShot(true, turn + FORGET_TURN));
	}

}

int RoundtripCpu::getConfidence() const {
	if (registed_.size() < MIN_BULLETS) return 0;

//	int cnt = std::count_if(registed_.begin(), registed_.end(),
//							sgi::select1st<RegistedShot>());

	int cnt = 0;
	for (std::deque<RegistedShot>::const_iterator ite = registed_.begin();
		 ite != registed_.end(); ++ite)
	{
		if ((*ite).first) cnt++;
	}

	return MAX_CONFIDENCE * cnt / registed_.size();

}
