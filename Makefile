DSRC=$(shell find src -name "*.d")
SOURCES=$(DSRC) import/SDL_video.d import/SDL_mixer.d
OBJS=$(SOURCES:.d=.o)
CFLAGS=
DFLAGS=-O -release -Iimport -Isrc -I/usr/X11R6/include/
EXE=tf
COBJS=src/dirent_d.o

all: $(EXE)

$(EXE): bulletml/libbulletml_d.a import/SDL_keysym_.d $(OBJS) $(COBJS)
	gcc $(CFLAGS) -o $@ $(OBJS) $(COBJS) bulletml/libbulletml_d.a -L/usr/local/lib -L/usr/lib -lphobos -lpthread -lGLU -lGL -lglut -lm -lstdc++ -lSDL -lSDL_mixer

$(OBJS): %.o: %.d
	dmd -d -c -of$@ $(DFLAGS) $<

src/dirent_d.o: src/dirent_d.c
	gcc -c $< -o $@

bulletml/libbulletml_d.a:
	make -C bulletss/bulletml

clean_bulletml:
	make -C bulletss/bulletml clean

clean: clean_bulletml
	rm -r src/abagames/tf/*.o