/**
 * $Id: chairman.cc,v 1.2 2003/09/16 08:25:57 i Exp $
 *
 * Copyright (C) shinichiro.h <s31552@mail.ecc.u-tokyo.ac.jp>
 *  http://user.ecc.u-tokyo.ac.jp/~s31552/wp/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "chairman.h"
#include "reflection.h"
#include "oblivion.h"
#include "veto.h"
#include "position.h"
#include "roundtrip.h"
#include "human.h"
#include "cpuutil.h"

//#include "profile.h"

#include <algorithm>
#include <functional>
#include <cassert>
#include <fstream>

ChairmanCpu::ChairmanCpu() {
	name_ = "chairman";

	diet_.push_back(new VetoCpu);
	diet_.push_back(new ReflectionCpu);
	diet_.push_back(new PositionCpu);
	diet_.push_back(new RoundtripCpu);
	diet_.push_back(new OblivionCpu);

/*
  std::ifstream is("save/cpu.conf");
  std::string str;
  while (is >> str) {
  if (str == "veto") diet_.push_back(new VetoCpu);
  else if (str == "reflection") diet_.push_back(new ReflectionCpu);
  else if (str == "oblivion") diet_.push_back(new OblivionCpu);
  else if (str == "position") diet_.push_back(new PositionCpu);
  else if (str == "roundtrip") diet_.push_back(new RoundtripCpu);
//		else if (str == "human") diet_.push_back(new HumanCpu);
}
*/
	assert(diet_.size() <= MAX_DIETS);

/*
  if (Game::instance()->screenIsLarge()) {
  const Rect& r = Screen::PREVIEW_FRAME;

  surf_.reset(new Surface(
  *Game::instance()->screen(),
  Rect(r.x()+4, r.y()+4, r.w()-8, r.h()-8, true)));
  }
*/
}

ChairmanCpu::~ChairmanCpu() {
	delete_clear(diet_);
}

void ChairmanCpu::calc() {
//	PROFILE(name().c_str());

	for (size_t j = 0; j < diet_.size(); j++) {
		CpuInputBase* cpu = diet_[j];

//		PROFILE(cpu->name().c_str());

		cpu->update();
		for (int i = 0; i < 9; i++) {
			int eval = cpu->getAxisEvaluation(i) * cpu->getConfidence();
			evaluations_[i] += eval;
			dietEvals_[j][i] = eval;
		}
		//std::cerr << evaluations_[i] << " ";
	}
	int index = std::max_element(evaluations_, evaluations_+9) - evaluations_;

//	axis_ = int::createFromSmallCode(index);
	axis_ = index;
	//std::cerr<<axis_.getSmallAxisCode()<<std::endl;
	decision_ = false;

//	if (surf_.get() != 0) drawGraph();
	drawGraph();
}

void ChairmanCpu::registShot(float x, float y, float sx, float sy) {
	for (size_t i = 0; i < diet_.size(); i++) {
		diet_[i]->registShot(x, y, sx, sy);
	}
}

#if 0
void ChairmanCpu::registEnemy(const class Enemy* enemy) {
	for (size_t i = 0; i < diet_.size(); i++) {
		diet_[i]->registEnemy(enemy);
	}
}
#endif

void ChairmanCpu::report() const {
	for (size_t i = 0; i < diet_.size(); i++) {
		diet_[i]->report();
	}
}

/*
  void ChairmanCpu::registEnemy(const Enemy* enemy) {
  for (size_t i = 0; i < diet_.size(); i++) {
  diet_[i]->registEnemy(enemy);
  }
  }
*/

#ifdef WINDOWS
#include <windows.h>
#endif
#include <GL/gl.h>

void ChairmanCpu::drawGraph() {
	// $BE,Ev$9$.$k$J$"(B
	static const float cols[][3] = {
		{ 1, 0, 0 }, { 1, 0.5, 0 }, { 1, 1, 0 }, { 0, 1, 0}, { 0, 0, 1 },
		{ 1, 0, 1 }, { 0.5, 0.5, 0.5 }
	};

	glBegin(GL_LINES);
	for (size_t i = 0; i < diet_.size(); i++) {
		drawSingleGraph(dietEvals_[i], cols[i]);
	}
	glEnd();

}

void ChairmanCpu::drawSingleGraph(int* evals, const float* col) {
	static const int cx = 320;
	static const int cy = 240;

	int ev = evals[8] / 20 + 20;
	if (ev > cx-1) ev = cx-1;
	else if (ev < 0) ev = 0;
	int px = static_cast<int>(axis2vec[8].x * ev) + cx;
	int py = static_cast<int>(axis2vec[8].y * ev) + cy;

	for (size_t i = 1; i < 9; i++) {
		int ev = evals[i] / 20 + 20;
		if (ev > 200) ev = 200;
		else if (ev < 0) ev = 0;
		int x = static_cast<int>(axis2vec[i].x * ev) + cx;
		int y = static_cast<int>(axis2vec[i].y * ev) + cy;
//		std::cout << evals[i] <<' '<<ev<< ' ' <<px << ' ' << py << ' ' << x << ' ' << y << std::endl;

		glColor3fv(col);
		glVertex3f(px, py, 0);
		glVertex3f(x, y, 0);

		px = x;
		py = y;
	}
}

