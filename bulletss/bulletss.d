import stream;
import math;
import string;
import random;
import date;
import path;

import c.stdio;

import SDL;
import opengl;

import bulletml;

import mymath;
import charactor;
import padinput;
import cpuinput;
import camera;
import dirent;

char[] ftoa(double f) {
	char[] buf = new char[256];
	int len = sprintf(buf, "%f", f);
	return buf[0 .. len];
}

extern(C) void srand(uint seed);

private:
const float VEL_SS_SDM_RATIO = 62.0 / 100;
const float VEL_SDM_SS_RATIO = 100.0 / 62;

extern (C) {

	double getBulletDirection_(BulletMLRunner* r) {
		return rtod(Bullet.now.angle());
	}
	double getAimDirection_(BulletMLRunner* r) {
		Bullet b = Bullet.now;
		Target t = BulletSS.obj.target();

		return 180-rtod(math.atan2(t.x()-b.x(), t.y()-b.y()));
	}
	double getBulletSpeed_(BulletMLRunner* r) {
		return Bullet.now.velocity() * VEL_SS_SDM_RATIO;
	}
	double getDefaultSpeed_(BulletMLRunner* r) {
		return 1;
	}
	double getRank_(BulletMLRunner* r) {
		return 0.5;
	}
	void createSimpleBullet_(BulletMLRunner* r, double d, double s) {
		BulletSS.obj.addShot(dtor(d), s * VEL_SDM_SS_RATIO);
	}
	void createBullet_(BulletMLRunner* r, BulletMLState* state,
					   double d, double s) {
		BulletSS.obj.addBullet(state, dtor(d), s * VEL_SDM_SS_RATIO);
	}

	int getTurn_(BulletMLRunner* r) {
		return BulletSS.obj.turn();
	}
	void doVanish_(BulletMLRunner* r) {
		Bullet.now.kill();
	}
	void doChangeDirection_(BulletMLRunner* r, double d) {
		Bullet.now.setAngle(dtor(d));
		Bullet.now.setCartesian();
	}
	void doChangeSpeed_(BulletMLRunner* r, double s) {
		Bullet.now.setVelocity(s * VEL_SDM_SS_RATIO);
		Bullet.now.setCartesian();
	}
	void doAccelX_(BulletMLRunner* r, double sx) {
		Bullet.now.setSX(sx * VEL_SDM_SS_RATIO);
		Bullet.now.setPolar();
	}
	void doAccelY_(BulletMLRunner* r, double sy) {
		Bullet.now.setSY(sy * VEL_SDM_SS_RATIO);
		Bullet.now.setPolar();
	}
	double getBulletSpeedX_(BulletMLRunner* r) {
		return Bullet.now.sx() * VEL_SS_SDM_RATIO;
	}
	double getBulletSpeedY_(BulletMLRunner* r) {
		return Bullet.now.sy() * VEL_SS_SDM_RATIO;
	}
}

public:

class BulletSS {
public:
	static BulletSS obj;

	void registFunctions(BulletMLRunner* runner) {
		BulletMLRunner_set_getBulletDirection(runner, &getBulletDirection_);
		BulletMLRunner_set_getAimDirection(runner, &getAimDirection_);
		BulletMLRunner_set_getBulletSpeed(runner, &getBulletSpeed_);
		BulletMLRunner_set_getDefaultSpeed(runner, &getDefaultSpeed_);
		BulletMLRunner_set_getRank(runner, &getRank_);
		BulletMLRunner_set_createSimpleBullet(runner, &createSimpleBullet_);
		BulletMLRunner_set_createBullet(runner, &createBullet_);
		BulletMLRunner_set_getTurn(runner, &getTurn_);
		BulletMLRunner_set_doVanish(runner, &doVanish_);

		BulletMLRunner_set_doChangeDirection(runner, &doChangeDirection_);
		BulletMLRunner_set_doChangeSpeed(runner, &doChangeSpeed_);
		BulletMLRunner_set_doAccelX(runner, &doAccelX_);
		BulletMLRunner_set_doAccelY(runner, &doAccelY_);
		BulletMLRunner_set_getBulletSpeedX(runner, &getBulletSpeedX_);
		BulletMLRunner_set_getBulletSpeedY(runner, &getBulletSpeedY_);
	}

	void initSDLOpenGL() {
		int w, h, bpp;
		SDL_VideoInfo* info;

		SDL_Init(SDL_INIT_VIDEO);

		w = 640;
		h = 480;
		info = SDL_GetVideoInfo();
		bpp = info.vfmt.BitsPerPixel;

		SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );  
		SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
		SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );  
		SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
		SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

		SDL_SetVideoMode(w, h, bpp, SDL_OPENGL/*|SDL_FULLSCREEN*/);

		glClearColor(0, 0, 0, 0);
		glViewport(0, 0, w, h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		float asp = cast(float)w / h;
		gluPerspective(45.0, asp, 0.1, 1000);
	}

	void procEndInput() {
		SDL_Event e;
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				end_ = true;
			}
			else if (e.type == SDL_KEYDOWN) {
				if (e.key.keysym.sym == SDLK_ESCAPE) {
					end_ = true;
				}
			}
		}
	}

	void drawField() {
		glBegin(GL_LINE_LOOP);
		glColor3f(1, 1, 1);
		glVertex3f(170, 40, 10);
		glVertex3f(170, 440, 10);
		glVertex3f(470, 440, 10);
		glVertex3f(470, 40, 10);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glColor3f(1, 1, 1);
		glVertex3f(170, 40, -20);
		glVertex3f(170, 440, -20);
		glVertex3f(470, 440, -20);
		glVertex3f(470, 40, -20);
		glEnd();

		glBegin(GL_LINES);
		glVertex3f(170, 40, 10);
		glVertex3f(170, 40, -20);
		glVertex3f(170, 440, 10);
		glVertex3f(170, 440, -20);
		glVertex3f(470, 440, 10);
		glVertex3f(470, 440, -20);
		glVertex3f(470, 40, 10);
		glVertex3f(470, 40, -20);
		glEnd();
	}

	void initXmls(char[] runFile) {
		if (path.getExt(runFile) == "xml") {
			xmls_ ~= runFile;
		}
		else {
			DIR* dir = opendir(string.toStringz(runFile));
			char* f_name;
			while ((f_name = readdir_filename(dir)) != null) {
				char[] name = string.toString(f_name);
				if (path.getExt(name) == "xml") {
					debug {
						stream.stdout.writeLine(name);
					}
					xmls_ ~= runFile ~ "/" ~ name;
				}
			}
			closedir(dir);
		}
	}

	int run(char[][] args) {
		obj = this;

		srand(getUTCtime());
		random.rand_seed(getUTCtime(), 0);

		charactors_ = new Charactor[CHAR_NUM];
		shotX_ = new float[CHAR_NUM];
		shotY_ = new float[CHAR_NUM];
		shotSX_ = new float[CHAR_NUM];
		shotSY_ = new float[CHAR_NUM];

		char[] runFile = "bosses.d/";
		if (args.length > 1) {
			runFile = args[1];
		}
		initXmls(runFile);

		initSDLOpenGL();

		camera_ = new Camera();

		end_ = false;

		target_ = new Target(150, 300, 0, 0, Charactor.Type.PLAYER);
		input_ = new CpuInput(target_);
		target_.setInput(input_);

		while (!end_) {
			char[] xml = xmls_[rnd(xmls_.length)];
			version(linux) {
				stream.stdout.writeLine(xml);
			}
			BulletMLParserTinyXML* parser =
				BulletMLParserTinyXML_new(string.toStringz(xml));
			BulletMLParserTinyXML_parse(parser);

			BulletMLRunner* runner = BulletMLRunner_new_parser(parser);
			registFunctions(runner);

			foreach (inout Charactor c; charactors_) {
				c = null;
			}

			topBullet_ = new Bullet(runner, 150, 100, 0, 0,
									Charactor.Type.BOSS, -1);
			charactors_[0] = topBullet_;

			turn_ = 0;
			int endTurn_ = 0;

			while (endTurn_ < 100 && !end_) {
				procEndInput();

				if (topBullet_.isEnd()) {
					endTurn_++;
				}

				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				camera_.set();

				target_.move();
				target_.draw();

				shotNum_ = 0;
				foreach (inout Charactor c; charactors_) {
					if (c) {
						c.move();
						if (c.alive()) {
							c.draw();

							shotX_[shotNum_] = c.x();
							shotY_[shotNum_] = c.y();
							shotSX_[shotNum_] = c.sx();
							shotSY_[shotNum_] = c.sy();
							shotNum_++;
						}
						else {
							delete c;
							c = null;
						}
					}
				}

				drawField();

				SDL_GL_SwapBuffers();
				SDL_Delay(16);

				turn_++;
			}

			BulletMLRunner_delete(runner);
			BulletMLParserTinyXML_delete(parser);
		}

		SDL_Quit();

		return 0;
	}

	Target target() { return target_; }
	Bullet topBullet() { return topBullet_; }

	void addShot(float a, float v) {
		foreach (inout Charactor c; charactors_) {
			if (!c) {
				if (Bullet.now.generation() & 1) {
					c = new Shot(Bullet.now.x(), Bullet.now.y(),
								 a, v, Charactor.Type.SHOT1);
				}
				else {
					c = new Shot(Bullet.now.x(), Bullet.now.y(),
								 a, v, Charactor.Type.SHOT2);
				}
				input_.registShot(c.x(), c.y(), c.sx(), c.sy());
				return;
			}
		}
	}
	void addBullet(BulletMLState* s, float a, float v) {
		foreach (inout Charactor c; charactors_) {
			if (!c) {
				BulletMLRunner* runner =
					BulletMLRunner_new_state(s);
				registFunctions(runner);
				c = new Bullet(runner, Bullet.now.x(), Bullet.now.y(),
							   a, v, Charactor.Type.BULLET,
							   Bullet.now.generation()+1);
				input_.registShot(c.x(), c.y(), c.sx(), c.sy());
				return;
			}
		}
	}

	int turn() { return turn_; }

	void getBullets(out int len, out float* x, out float* y,
					out float* sx, out float* sy) {
		len = shotNum_;
		x = shotX_;
		y = shotY_;
		sx = shotSX_;
		sy = shotSY_;
	}

private:
	static const int CHAR_NUM = 1000;

	Target target_;
	Bullet topBullet_;
	CpuInput input_;

	Charactor[] charactors_;

	int turn_;

	bit end_;

	Camera camera_;

	char[][] xmls_;

private:
	int shotNum_;
	float[] shotX_, shotY_, shotSX_, shotSY_;

}

version (linux) {
	int main(char[][] args)	{
		auto BulletSS bulletss = new BulletSS;
		return bulletss.run(args);
	}
}
version (Win32) {
	extern(C) int doit(char[][] args) {
		auto BulletSS bulletss = new BulletSS;
		return bulletss.run(args);
	}
}

