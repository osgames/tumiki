/**
 * $Id: cpuinfo_sdmkun.h,v 1.1 2003/09/06 22:53:28 i Exp $
 *
 * Copyright (C) shinichiro.h <s31552@mail.ecc.u-tokyo.ac.jp>
 *  http://user.ecc.u-tokyo.ac.jp/~s31552/wp/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef cpuinfo_sdmkun_h_
#define cpuinfo_sdmkun_h_

#include "game.h"
#include "conf.h"

/// 白い弾幕くんのばあい。
class CpuInfoSdmkun : public CpuInfo {
public:
	/// player の大きさ。(当り判定)
	virtual double getPlayerSize() {
		return Conf::instance()->playerSize();
	}

	/// player の位置
	virtual Point getPlayerPnt() {
		return Game::instance()->player()->pnt();
	}

	/// player の速度
	virtual double getPlayerSpd() {
		return Conf::instance()->playerSpeed();
	}

	/// player の移動できる最大座標値。
	virtual Point getPlayerMaxPnt() {
		return Game::instance()->player()->upperPoint();
	}

	/// Bullet の位置と速度情報をつっこんで返す。
	virtual void getBulletsPosAndSpd(PosSpd& posspd) {
		Game::instance()->getEnemyPosAndSpd(posspd);
	}

	/// FPS
	virtual double getFps() {
		return Conf::instance()->fps();
	}

	/// 現在のターン数
	virtual int getTurn() {
		return Game::instance()->getTurn();
	}

	/// SPF
	virtual double getSpf() {
		return Game::instance()->spf();
	}

};

#endif // ! cpuinfo_sdmkun_h_

