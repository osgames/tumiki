#include "d_cpp_interface.h"

#include "util.h"
#include "cpuinfo.h"
#include "chairman.h"

D_CPP_BASE_CLASS_OPEN(CpuInfo, CpuInfoCave)
D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, getPlayerSize, double)
//D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, CpuInfoCave_getPlayerPnt, Point)
D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, getPlayerSpd, double)
//D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, CpuInfoCave_getPlayerMaxPnt, Point)
D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, getFps, double)
D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, getTurn, int)
D_CPP_VIRTUAL_METHOD_0(CpuInfoCave, getSpf, double)

float getPlayerPnt_x;
float getPlayerPnt_y;
virtual Point getPlayerPnt() {
	return Point(getPlayerPnt_x, getPlayerPnt_y);
}
void setPlayerPnt(float x, float y) {
	getPlayerPnt_x = x;
	getPlayerPnt_y = y;
}

float getPlayerMaxPnt_x;
float getPlayerMaxPnt_y;
virtual Point getPlayerMaxPnt() {
	return Point(getPlayerMaxPnt_x, getPlayerMaxPnt_y);
}
void setPlayerMaxPnt(float x, float y) {
	getPlayerMaxPnt_x = x;
	getPlayerMaxPnt_y = y;
}

int getBulletsPosSpd_len;
float* getBulletsPosSpd_pntx;
float* getBulletsPosSpd_pnty;
float* getBulletsPosSpd_spdx;
float* getBulletsPosSpd_spdy;
void setBulletsPosSpd(int len, float* px, float* py, float* sx, float* sy) {
	getBulletsPosSpd_len = len;
	getBulletsPosSpd_pntx = px;
	getBulletsPosSpd_pnty = py;
	getBulletsPosSpd_spdx = sx;
	getBulletsPosSpd_spdy = sy;
}
virtual void getBulletsPosAndSpd(PosSpd& posspd) {
	posspd.resize(getBulletsPosSpd_len);
	for (int i = 0; i < getBulletsPosSpd_len; i++) {
		posspd[i] = std::make_pair(Point(getBulletsPosSpd_pntx[i],
										 getBulletsPosSpd_pnty[i]),
								   Point(getBulletsPosSpd_spdx[i],
										 getBulletsPosSpd_spdy[i]));
	}
}
D_CPP_BASE_CLASS_CLOSE()

extern "C" {
	D_CPP_CLASS(CpuInfo, CpuInfo)
	D_CPP_CLASS(CpuInfoCave, CpuInfoCave)
	D_CPP_NEW_0(CpuInfoCave, CpuInfoCave_new)
	D_CPP_DELETE(CpuInfoCave, CpuInfoCave_delete)

	D_CPP_METHOD_2(CpuInfoCave, setPlayerPnt, CpuInfo_setPlayerPnt, void, float, float)
	D_CPP_METHOD_2(CpuInfoCave, setPlayerMaxPnt, CpuInfo_setPlayerMaxPnt, void, float, float)
	D_CPP_METHOD_5(CpuInfoCave, setBulletsPosSpd, CpuInfo_setBulletsPosSpd, void, int, float*, float*, float*, float*)

	D_CPP_VIRTUAL_METHOD_SETTER_0(CpuInfoCave, getPlayerSize, CpuInfo_set_getPlayerSize, double)
	D_CPP_VIRTUAL_METHOD_SETTER_0(CpuInfoCave, getPlayerSpd, CpuInfo_set_getPlayerSpd, double)
	D_CPP_VIRTUAL_METHOD_SETTER_0(CpuInfoCave, getFps, CpuInfo_set_getFps, double)
	D_CPP_VIRTUAL_METHOD_SETTER_0(CpuInfoCave, getTurn, CpuInfo_set_getTurn, int)
	D_CPP_VIRTUAL_METHOD_SETTER_0(CpuInfoCave, getSpf, CpuInfo_set_getSpf, double)

}

extern "C" {
	D_CPP_CLASS(CpuInputBase, CpuInputBase)
	D_CPP_DELETE(CpuInputBase, Cpu_delete)
	D_CPP_STATIC_METHOD_0(CpuInputBase, getDefaultCpu, Cpu_getDefaultCpu, CpuInputBase*)
	D_CPP_STATIC_METHOD_1(CpuInputBase, setCpuInfomation, Cpu_setCpuInfomation, void, CpuInfo*)
	D_CPP_METHOD_4(CpuInputBase, registShot, Cpu_registShot, void, float, float, float, float)
	D_CPP_METHOD_0(CpuInputBase, getAxis, Cpu_getAxis, int)
}

