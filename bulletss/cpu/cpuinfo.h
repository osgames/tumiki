/**
 * $Id: cpuinfo.h,v 1.2 2003/09/16 08:25:57 i Exp $
 *
 * Copyright (C) shinichiro.h <s31552@mail.ecc.u-tokyo.ac.jp>
 *  http://user.ecc.u-tokyo.ac.jp/~s31552/wp/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef cpuinfo_h_
#define cpuinfo_h_

#include "util.h"

#include <utility>
#include <vector>

/// cpu にくれてやるべき情報群。純粋仮想。
class CpuInfo {
public:
	typedef std::vector<std::pair<Point, Point> > PosSpd;

	/// player の大きさ。
	virtual double getPlayerSize() =0;

	/// player の位置
	virtual Point getPlayerPnt() =0;

	/// player の速度
	virtual double getPlayerSpd() =0;

	/// player の移動できる最大座標値。
	virtual Point getPlayerMaxPnt() =0;

	/// Bullet の位置と速度情報をつっこんで返す。
	virtual void getBulletsPosAndSpd(PosSpd& posspd) =0;

	/// FPS
	virtual double getFps() =0;

	/// 現在のターン数
	virtual int getTurn() =0;

	/// SPF オーバーライドしなくて良し。
	virtual double getSpf() {
		return 1.0 / getFps();
	}

};

#endif // ! cpuinfo_h_



