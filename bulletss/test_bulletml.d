import bulletml;

import stream;
import string;
import c.stdio;

import random;
import date;

private:

int t;

char[] ftoa(double f) {
	char[] buf = new char[256];
	int len = sprintf(buf, "%f", f);
	return buf[0 .. len];
}

extern (C) {
	double getBulletDirection_(BulletMLRunner* r) {
		stream.stdout.writeLine("getBulletDirection");
		return 0;
	}
	double getAimDirection_(BulletMLRunner* r) {
		stream.stdout.writeLine("getAimDirection");
		return 0;
	}
	double getBulletSpeed_(BulletMLRunner* r) {
		stream.stdout.writeLine("getBulletSpeed");
		return 0;
	}
	double getDefaultSpeed_(BulletMLRunner* r) {
		stream.stdout.writeLine("getDefaultSpeed");
		return 0;
	}
	double getRank_(BulletMLRunner* r) {
		stream.stdout.writeLine("getRank");
		return 0;
	}
	void createSimpleBullet_(BulletMLRunner* r, double d, double s) {
		stream.stdout.writeLine(
			"createSimpleBullet " ~ ftoa(d) ~ "," ~ ftoa(s));
	}
	void createBullet_(BulletMLRunner* r, BulletMLState* state,
					  double d, double s) {
		stream.stdout.writeLine("createBullet " ~ ftoa(d) ~ "," ~ ftoa(s));
	}

	int getTurn_(BulletMLRunner* r) {
		stream.stdout.writeLine("getTurn " ~ string.toString(t));
		return t;
	}
	void doVanish_(BulletMLRunner* r) {
		stream.stdout.writeLine("doVanish");
	}
	void doChangeDirection_(BulletMLRunner* r, double d) {
		stream.stdout.writeLine("doChangeDirection " ~ ftoa(d));
	}
	void doChangeSpeed_(BulletMLRunner* r, double s) {
		stream.stdout.writeLine("doChangeSpeed " ~ ftoa(s));
	}
	void doAccelX_(BulletMLRunner* r, double x) {
		stream.stdout.writeLine("doAccelX " ~ ftoa(x));
	}
	void doAccelY_(BulletMLRunner* r, double y) {
		stream.stdout.writeLine("doAccelY " ~ ftoa(y));
	}
	double getBulletSpeedX_(BulletMLRunner* r) {
		stream.stdout.writeLine("getBulletSpeedX");
		return 0;
	}
	double getBulletSpeedY_(BulletMLRunner* r) {
		stream.stdout.writeLine("getBulletSpeedY");
		return 0;
	}
	double getRand_(BulletMLRunner* r) {
		return (cast(double)(random.rand() & 32767)) / 32767.0;
	}

}

void registerFunctions(BulletMLRunner* runner) {
	BulletMLRunner_set_getBulletDirection(runner, &getBulletDirection_);
	BulletMLRunner_set_getAimDirection(runner, &getAimDirection_);
	BulletMLRunner_set_getBulletSpeed(runner, &getBulletSpeed_);
	BulletMLRunner_set_getDefaultSpeed(runner, &getDefaultSpeed_);
	BulletMLRunner_set_getRank(runner, &getRank_);
	BulletMLRunner_set_createSimpleBullet(runner, &createSimpleBullet_);
	BulletMLRunner_set_createBullet(runner, &createBullet_);
	BulletMLRunner_set_getTurn(runner, &getTurn_);
	BulletMLRunner_set_doVanish(runner, &doVanish_);

	BulletMLRunner_set_doChangeDirection(runner, &doChangeDirection_);
	BulletMLRunner_set_doChangeSpeed(runner, &doChangeSpeed_);
	BulletMLRunner_set_doAccelX(runner, &doAccelX_);
	BulletMLRunner_set_doAccelY(runner, &doAccelY_);
	BulletMLRunner_set_getBulletSpeedX(runner, &getBulletSpeedX_);
	BulletMLRunner_set_getBulletSpeedY(runner, &getBulletSpeedY_);
	BulletMLRunner_set_getRand(runner, &getRand_);
}

public:

int main(char args[][]) {
	rand_seed(getUTCtime(), 0);

	if (args.length == 1) {
		stream.stdout.writeLine("Usage: " ~ args[0] ~ " xml_file");
		return 0;
	}

	BulletMLParserTinyXML* parser =
		BulletMLParserTinyXML_new(string.toStringz(args[1]));
	BulletMLParserTinyXML_parse(parser);

	BulletMLRunner* runner = BulletMLRunner_new_parser(parser);
	registerFunctions(runner);

	// 1000 frames
	for (t = 0; t < 1000; t++) {
		if (BulletMLRunner_isEnd(runner)) {
			stream.stdout.writeLine("end");
			break;
		}
		BulletMLRunner_run(runner);
	}

	BulletMLRunner_delete(runner);
	BulletMLParserTinyXML_delete(parser);

	return 0;
}

