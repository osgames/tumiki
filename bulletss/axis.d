class Axis {
public:
	static const int NONE = 0;
	static const int UP = 1;
	static const int UPRIGHT = 5;
	static const int RIGHT = 4;
	static const int DOWNRIGHT = 6;
	static const int DOWN = 2;
	static const int DOWNLEFT = 10;
	static const int LEFT = 8;
	static const int UPLEFT = 9;

public:
	this() {
		axis_ = NONE;
	}
	this(int axis) {
		axis_ = axis;
	}
public:
	void add(int axis) { axis_ |= axis; }
	void add(Axis axis) { add(axis.axis_); }

public:
	char[] getName() {
        if (axis_ == NONE) return "NONE";
        else if (axis_ == UP) return "UP";
        else if (axis_ == UPRIGHT) return "UPRIGHT";
        else if (axis_ == RIGHT) return "RIGHT";
        else if (axis_ == DOWNRIGHT) return "DOWNRIGHT";
        else if (axis_ == DOWN) return "DOWN";
        else if (axis_ == DOWNLEFT) return "DOWNLEFT";
        else if (axis_ == LEFT) return "LEFT";
        else if (axis_ == UPLEFT) return "UPLEFT";
        else return "UNKNOWN";
	}

public:
	int getAxisCode() { return axis_; }
	int getSmallAxisCode() {
		if (axis_ == NONE) return 0;
		else if (axis_ == UP) return 1;
		else if (axis_ == UPRIGHT) return 2;
		else if (axis_ == RIGHT) return 3;
		else if (axis_ == DOWNRIGHT) return 4;
		else if (axis_ == DOWN) return 5;
		else if (axis_ == DOWNLEFT) return 6;
		else if (axis_ == LEFT) return 7;
		else if (axis_ == UPLEFT) return 8;
		else return 0;
	}
	static int createFromSmallCode(int code) {
		if (code == 0) return NONE;
		else if (code == 1) return UP;
		else if (code == 2) return UPRIGHT;
		else if (code == 3) return RIGHT;
		else if (code == 4) return DOWNRIGHT;
		else if (code == 5) return DOWN;
		else if (code == 6) return DOWNLEFT;
		else if (code == 7) return LEFT;
		else if (code == 8) return UPLEFT;
		else return NONE;
	}

public:
	bit eq(Axis rhs) { return axis_ == rhs.axis_; }
	bit eq(int rhs) { return axis_ == rhs; }
public:
	bit isRight() {
		return (axis_ & RIGHT) != 0;
	}
	bit isLeft() {
		return (axis_ & LEFT) != 0;
	}
	bit isDown() {
		return (axis_ & DOWN) != 0;
	}
	bit isUp() {
		return (axis_ & UP) != 0;
	}
	bit isXAxis() {
		return isRight() || isLeft();
	}
	bit isYAxis() {
		return isUp() || isDown();
	}

private:
	int axis_;
};
