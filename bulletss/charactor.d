import math;
import stream;

import opengl;

import bulletml;

import mymath;
import padinput;
import math;

class Charactor {
public:
	enum Type { BOSS, BULLET, SHOT1, SHOT2, PLAYER }

public:
	this(float x, float y, float a, float v, Type type) {
		x_ = x;
		y_ = y;

		a_ = a;
		v_ = v;

		sx_ = v * math.sin(a);
		sy_ = - v * math.cos(a);

		type_ = type;
		turn_ = 0;

		alive_ = true;
	}

	void move() {
		x_ += sx_;
		y_ += sy_;
		turn_++;
		if ((x_ < 0 || y_ < 0 || x_ > 300 || y_ > 400) &&
			type_ != Type.BOSS && type_ != Type.PLAYER) kill();
	}

	void draw() {
		switch (type_) {
		case Type.BOSS: {
			static int size = 5;

			glPushMatrix();
			glTranslatef(x_+170, y_+40, 0);
			glRotatef(turn_*5, 0, 0, 1);
			glRotatef(turn_*3, 0, 1, 0);

			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(-size, -size, 0);
			glVertex3f(size, -size, 0);
			glVertex3f(size, size, 0);
			glVertex3f(-size, size, 0);
			glEnd();
			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(-size, 0, -size);
			glVertex3f(size, 0, -size);
			glVertex3f(size, 0, size);
			glVertex3f(-size, 0, size);
			glEnd();
			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(0, -size, -size);
			glVertex3f(0, size, -size);
			glVertex3f(0, size, size);
			glVertex3f(0, -size, size);
			glEnd();

			glPopMatrix();

			break;
		}
		case Type.SHOT1: {
			glPushMatrix();
			glTranslatef(x_+170, y_+40, 0);
			glRotatef(rtod(a_), 0, 0, 1);
			glRotatef(turn_*5, 0, 1, 0);

			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(-2, 3, 0);
			glVertex3f(2, 3, 0);
			glVertex3f(0, -3, 0);
			glEnd();

			glPopMatrix();

			break;
		}
		case Type.SHOT2: {
			glPushMatrix();
			glTranslatef(x_+170, y_+40, 0);
			glRotatef(rtod(a_), 0, 0, 1);
			glRotatef(-turn_*5, 0, 1, 0);

			glBegin(GL_TRIANGLE_STRIP);
			glColor3f(1, 1, 1);
			glVertex3f(-2, 3, 0);
			glVertex3f(2, 3, 0);
			glVertex3f(0, -3, 0);
			glEnd();

			glPopMatrix();

			break;
		}
		case Type.BULLET: {
			static int size = 3;

			glPushMatrix();
			glTranslatef(x_+170, y_+40, 0);
			glRotatef(turn_*5, 0, 0, 1);
			glRotatef(turn_*3, 0, 1, 0);

			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(-size, -size, 0);
			glVertex3f(size, -size, 0);
			glVertex3f(size, size, 0);
			glVertex3f(-size, size, 0);
			glEnd();

			glPopMatrix();

			break;
		}
		case Type.PLAYER: {
			static const int size = 5;
			static const float sqrt3 = 1.732;

			glPushMatrix();
			glTranslatef(x_+170, y_+40, 0);
			glRotatef(-turn_*5, 0, 0, 1);
			glRotatef(turn_*3, 0, 1, 0);

			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(-size, size/sqrt3, 0);
			glVertex3f(size, size/sqrt3, 0);
			glVertex3f(0, -2*size/sqrt3, 0);
			glEnd();

			glPopMatrix();

			glPushMatrix();
			glTranslatef(x_+170, y_+40, 0);
			glRotatef(turn_*5, 0, 0, 1);
			glRotatef(-turn_*3, 0, 1, 0);

			glBegin(GL_LINE_LOOP);
			glColor3f(1, 1, 1);
			glVertex3f(-size, -size/sqrt3, 0);
			glVertex3f(size, -size/sqrt3, 0);
			glVertex3f(0, 2*size/sqrt3, 0);
			glEnd();

			glPopMatrix();

			break;
		}
		default:
			break;
		}
	}

	float x() {
		return x_;
	}
	float y() {
		return y_;
	}

	float sx() {
		return sx_;
	}
	float sy() {
		return sy_;
	}

	bit alive() { return alive_; }
	void kill() { alive_ = false; }

	int turn() { return turn_; }

private:
	float x_, y_, sx_, sy_;
	float a_, v_;
	Type type_;
	int turn_;
	bit alive_;

}

class Shot : public Charactor {
public:
	this(float x, float y, float a, float v, Type type) {
		super(x, y, a, v, type);
	}

}

class Bullet : public Shot {
public:
	this(BulletMLRunner* runner, float x, float y,
		 float a, float v, Type type, int gen) {
		super(x, y, a, v, type);
		runner_ = runner;
		a_ = a;
		v_ = v;
		generation_ = gen;
	}

	void move() {
		now = this;
		if (!BulletMLRunner_isEnd(runner_)) {
			BulletMLRunner_run(runner_);
		}
		super.move();
	}

	bit isEnd() {
		return BulletMLRunner_isEnd(runner_);
	}

	float angle() { return a_; }
	float velocity() { return v_; }

	void setCartesian() {
		sx_ = v_ * math.sin(a_);
		sy_ = - v_ * math.cos(a_);
	}
	void setPolar() {
		a_ = math.atan2(sx_, -sy_);
		v_ = math.sqrt(sx_*sx_+sy_*sy_);
	}

	void setSX(float sx) {
		sx_ = sx;
	}
	void setSY(float sy) {
		sy_ = sy;
	}
	void setAngle(float a) {
		a_ = a;
	}
	void setVelocity(float v) {
		v_ = v;
	}

	int generation() {
		return generation_;
	}

public:
	static Bullet now;

private:
	BulletMLRunner* runner_;
	int generation_;

}

class Target : public Charactor {
public:
	this(float x, float y, float a, float v, Type type) {
		super(x, y, a, v, type);
	}

	void setInput(PadInput input) {
		input_ = input;
	}

	void move() {
		turn_++;

		auto Axis axis = input_.getAxis();

		float base = 2;
		float cross = 2 / math.sqrt(2.0);

		if (axis == Axis.UP) y_ -= base;
		else if (axis == Axis.RIGHT) x_ += base;
		else if (axis == Axis.DOWN) y_ += base;
		else if (axis == Axis.LEFT) x_ -= base;
		else if (axis == Axis.UPRIGHT) {
			x_ += cross;
			y_ -= cross;
		}
		else if (axis == Axis.DOWNRIGHT) {
			x_ += cross;
			y_ += cross;
		}
		else if (axis == Axis.DOWNLEFT) {
			x_ -= cross;
			y_ += cross;
		}
		else if (axis == Axis.UPLEFT) {
			x_ -= cross;
			y_ -= cross;
		}
	}

private:
	PadInput input_;

}

char[] ftoa(double f) {
	char[] buf = new char[256];
	int len = sprintf(buf, "%f", f);
	return buf[0 .. len];
}

