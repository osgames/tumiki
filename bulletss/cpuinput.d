import string;

import cpu;
import axis;
import charactor;
import padinput;
import bulletss;

private:
struct CpuInfoHolder {
	Target player;
}
CpuInfoHolder cpuInfoHolder_;

extern (C) {
	double CpuInfo_getPlayerSize_(CpuInfo* info) {
		return 20;
	}
	double CpuInfo_getPlayerSpd_(CpuInfo* info) {
		return 3;
	}
	double CpuInfo_getFps_(CpuInfo* info) {
		return 1;
	}
	int CpuInfo_getTurn_(CpuInfo* info) {
		return cpuInfoHolder_.player.turn();
	}
	double CpuInfo_getSpf_(CpuInfo* info) {
		return 1;
	}
}

public:

class CpuInput : public PadInput {
public:
	this(Target player) {
		player_ = player;
		cpu_ = Cpu_getDefaultCpu();
		info_ = CpuInfoCave_new();

		cpuInfoHolder_.player = player;

		CpuInfo_set_getPlayerSize(info_, &CpuInfo_getPlayerSize_);
		CpuInfo_set_getPlayerSpd(info_, &CpuInfo_getPlayerSpd_);
		CpuInfo_set_getFps(info_, &CpuInfo_getFps_);
		CpuInfo_set_getTurn(info_, &CpuInfo_getTurn_);
		CpuInfo_set_getSpf(info_, &CpuInfo_getSpf_);
		CpuInfo_setPlayerMaxPnt(info_, 300, 400);

		Cpu_setCpuInfomation(info_);
	}
	~this() {
		Cpu_delete(cpu_);
		CpuInfoCave_delete(info_);
	}

	Axis getAxis() {
		CpuInfo_setPlayerPnt(info_, player_.x(), player_.y());
		int len;
		float* x, y, sx, sy;

		BulletSS.obj.getBullets(len, x, y, sx, sy);

		CpuInfo_setBulletsPosSpd(info_, len, x, y, sx, sy);

		int a = Cpu_getAxis(cpu_);
		return new Axis(Axis.createFromSmallCode(a));
	}
	bit getButton(int id) {
		return false;
	}

	void registShot(float x, float y, float sx, float sy) {
		Cpu_registShot(cpu_, x, y, sx, sy);
	}

private:
	Target player_;
	CpuInputBase* cpu_;
	CpuInfoCave* info_;
}


